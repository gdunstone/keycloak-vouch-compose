# keycloak-vouch-compose

docker-compose project for a keycloak server with instructions for deploying vouch to protect nginx

runs from a .env with the following variables:

```
LETSENCRYPT_ACCOUNT_EMAIL=your.email@email.com
KEYCLOAK_PASSWORD=<admin password>
DOMAIN=your.domain.tld
KEYCLOAK_REALM="realm-name"
```

Your realm name can be just "master" or whatever you set it to in keycloak